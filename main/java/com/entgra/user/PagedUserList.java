package com.entgra.user;

import java.util.ArrayList;

public interface PagedUserList {

    public ArrayList<String> getUsers();

    public PageControl getPageControl();

    public boolean hasMorePages();

}
