package com.entgra.user.ldap;

import com.entgra.user.PageControl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

public class LDAPPageControl implements PageControl {

    private byte[] cookie = null;
    private LdapContext ctx = null;
    private static Log log = LogFactory.getLog(LDAPPageControl.class);

    public LDAPPageControl() {
    }

    public LDAPPageControl(byte[] cookie, LdapContext ctx) {
        this.cookie = cookie;
        this.ctx = ctx;
    }

    public void setLdapContext(LdapContext ctx) {
        this.ctx = ctx;
    }

    public byte[] getCookie() {
        return cookie;
    }

    public void setCookie(byte[] cookie) {
        this.cookie = cookie;
    }

    public LdapContext getLdapContext() { return ctx; }

    public void cleanUp() {
        try {
            if(ctx !=null) {
                ctx.close();
            }
        } catch (NamingException e) {
            log.error("Error closing the connection:" + e.getMessage(), e);
        }
    }

}
