package com.entgra.user;

import com.entgra.user.ldap.LDAPPageControl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wso2.carbon.user.core.UserStoreException;
import org.wso2.carbon.user.core.config.RealmConfiguration;
import org.wso2.carbon.user.core.ldap.LDAPConstants;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.Hashtable;

public class PageControlBuilder {

    public static final String PAGE_CONTROL_CLASS = "PageControlClass";

    private static Log log = LogFactory.getLog(PageControlBuilder.class);


    public static PageControl buildPageControl(RealmConfiguration realmConfig) throws UserStoreException {
        String controlName =  realmConfig.getUserStoreProperty(PAGE_CONTROL_CLASS);

        try {
            Class pageControlClass = Class.forName(controlName);
            PageControl pageControl = (PageControl) pageControlClass.newInstance();

            if(pageControl instanceof LDAPPageControl) {
                LdapContext ctx = getLDAPContext(realmConfig);
                LDAPPageControl ldapControl = (LDAPPageControl) pageControl;
                ldapControl.setLdapContext(ctx);

            }
            return pageControl;
        } catch (Exception e) {
            log.error("Error while building PageControl class:" + e.getMessage() , e);
            throw new UserStoreException("Error while building PageControl class:" + e.getMessage() , e);

        }
    }

    //TODO: Implement a context pool if relavent
    public static LdapContext getLDAPContext(org.wso2.carbon.user.api.RealmConfiguration realmConfig) throws UserStoreException{

        try {
            Hashtable<String, Object> pagedEnvironment = new Hashtable<String, Object>(11);
            pagedEnvironment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            pagedEnvironment.put("java.naming.security.authentication", "simple");
            //   pagedEnvironment.put("org.wso2.carbon.context.RequestBaseContext", "true");

            String connectionName = realmConfig.getUserStoreProperty(LDAPConstants.CONNECTION_NAME);
            if (connectionName != null) {
                pagedEnvironment.put("java.naming.security.principal", connectionName);
            }

            String conectionPassword =  realmConfig.getUserStoreProperty(LDAPConstants.CONNECTION_PASSWORD);
            if (conectionPassword != null) {
                pagedEnvironment.put("java.naming.security.credentials", conectionPassword);
            }


            String conectionURL =  realmConfig.getUserStoreProperty(LDAPConstants.CONNECTION_URL);
            if (conectionURL != null) {
                pagedEnvironment.put("java.naming.provider.url", conectionURL);
            }

            LdapContext ctx = new InitialLdapContext(pagedEnvironment, null);
            return ctx;
        } catch (NamingException e) {
            log.error("Error while building LDAPContext:" + e.getMessage() , e);
            throw new UserStoreException("Error while building LDAPContext:" + e.getMessage() , e);
        }
    }




}
